// theme.service.ts
export const enableDarkMode = () => {
  document.body.classList.add('dark');
};

export const disableDarkMode = () => {
  document.body.classList.remove('dark');
};

export const toggleDarkMode = (isDarkMode: any) => {
  if (isDarkMode) {
    enableDarkMode();
  } else {
    disableDarkMode();
  }
};
