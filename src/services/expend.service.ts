import axios from "axios";
export const list = async () => {
    try {
      const response = await axios.get('https://api.nasa.gov/planetary/apod?api_key=ed8RqQPwwW5tnRGz8TDudAmHtL5GiKoPzOQd1Uzy&count=10');
      return response.data; 
    } catch (error) {
      console.error('Error al obtener el tema de la API:', error);
      return null;
    }
  };